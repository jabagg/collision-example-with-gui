extends MarginContainer


#making variables for the labels for easy access
onready var pos_label = $counters/position
onready var speed_label = $counters/speed
onready var accel_label = $counters/acceleration

var box_pos
var box_speed
var box_accel



# Called when the node enters the scene tree for the first time.
func _ready():
	box_pos = $"../KinematicBody".translation.y
	pos_label.text = str("position: ", box_pos)
	
	box_speed = $"../KinematicBody".velocity.y
	speed_label.text = str("speed: ", box_speed)
	
	box_accel = $"../KinematicBody".gravity
	accel_label.text = str("acceleration ", box_accel)

#	box_accel = $"../KinematicBody".accel
#	accel_label.text = str("acceleration ", box_accel)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	box_pos = $"../KinematicBody".translation.y
	pos_label.text = str("position: ", box_pos)
	
	box_speed = $"../KinematicBody".velocity.y
	speed_label.text = str("speed: ", box_speed)
	
	box_accel = $"../KinematicBody".gravity
	accel_label.text = str("acceleration ", box_accel)
	
#	box_accel = $"../KinematicBody".accel
#	accel_label.text = str("acceleration ", box_accel)
	pass
